<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP Introduction</title>
</head>
<body>
	<h1>Full Address</h1>

	<?php

		function getFullAddress($specificAddress, $city, $province, $country) {
			echo "$specificAddress, $city, $province, $country";
		}

		getFullAddress("B15 L10 Magsaysay", "San Pedro City", "Laguna", "Philippines");
		echo "<br/>";
		getFullAddress("B5 L3 Bagumbayan", "Tanauan City", "Batangas", "Philippines");

	?>
</body>
</html>